#include <ros/ros.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>

#include <linux/videodev2.h>
#include <sys/ioctl.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static int tioctl(int fd, unsigned int request, void *arg)
{
    int r;
    if( fd < 0 ) return -1;

    do {
        r = ioctl(fd, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

int main( int argc, char **argv )
{
    ros::init(argc, argv, "test_v4l_mwe");

    // If uncommented, ioctls that follow will have sporadic EBUSY errors 
    ros::NodeHandle nh;

    std::string device="/dev/video1";
    const int width=1920;
    const int height=1216;

    struct stat st;

    if (-1 == stat(device.c_str(), &st)) {
        std::cerr  << "Cannot identify device " << device << "(" << errno << "):" << strerror(errno); 
        return -1;
    }

    if (!S_ISCHR(st.st_mode)) {
        std::cerr  << device << "is not a device "; 
        return -1;
    }

    int fd = open(device.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);
    if (-1 == fd) {
        std::cerr  << "Cannot open device " << device << "(" << errno << "):" << strerror(errno); 
        return -1;
    }

    // Immediately request buffers
    {
        struct v4l2_requestbuffers req;
        CLEAR(req);

        req.count = 4;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;

        if (-1 == tioctl(fd, VIDIOC_REQBUFS, &req)) {
            std::cerr << "Error requesting buffers for " << device << " (" << errno << "):" << strerror(errno);
            return -1;
        }
    }

    struct v4l2_capability cap;

    if (-1 == tioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            std::cerr << device << " is not a V4L2 device";
            return -1;
        } else {
            std::cerr << "Error querying capabilities for device " << device << "(" << errno << "):" << strerror(errno); 
            return -1;
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        std::cerr << device << " is not a video capture device";
        return -1;
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        std::cerr << device << " does not support streaming I/O";
        return -1;
    }

    struct v4l2_cropcap cropcap;
    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (0 == tioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
        struct v4l2_crop crop;
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        if (-1 == tioctl(fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
            case EINVAL:
                    /* Cropping not supported. */
                    break;
            default:
                    /* Errors ignored. */
                    break;
            }
        }
    } else {
        /* Errors ignored. */
    }

    // // Explicitly stop streaming
    // {
    //     enum v4l2_buf_type type;
    //     type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    //     if (-1 == tioctl(fd,VIDIOC_STREAMOFF, &type)) {
    //         std::cerr << "Error halting streaming for " << device << " (" << errno << "):" << strerror(errno);
    //         return -1;
    //     }
    // }

    // Free any existing buffers
    {
        struct v4l2_requestbuffers req;
        CLEAR(req);

        req.count = 0;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;

        if (-1 == tioctl(fd, VIDIOC_REQBUFS, &req)) {
            std::cerr << "Error freeing buffers for " << device << " (" << errno << "):" << strerror(errno);
            return -1;
        }
    }

    struct v4l2_format fmt;
    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if ( (width > 0) && (height > 0) ) {
        std::cerr << "Setting format for " << device << " to " << width << " x " << height << std::endl;

        fmt.fmt.pix.width       = width; 
        fmt.fmt.pix.height      = height; 
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY; 
        fmt.fmt.pix.field       = V4L2_FIELD_ANY;

        if (-1 == tioctl(fd, VIDIOC_S_FMT, &fmt)) {
            std::cerr << "Error setting format for device " << device << "(" << errno << "):" << strerror(errno); 
            return -1;
        }
        /* Note VIDIOC_S_FMT may change width and height. */
    }

    /* Regardless of whether we try to set the video parameters, read them back. */
    if (-1 == tioctl(fd, VIDIOC_G_FMT, &fmt)) {
        std::cerr << "Error querying format for device " << device << "(" << errno << "):" << strerror(errno); 
        return -1;
    }

    std::cerr << "Successfully created camera for " << device << " at " << fmt.fmt.pix.width << " x " << fmt.fmt.pix.height << std::endl;

    return 0;
}