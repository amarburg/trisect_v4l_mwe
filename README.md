Minimum working example of issues with V4L2 access to CSI cameras in ROS.

My setup:

* Xavier NX Development board
* AntMicro / Capable Robot Nano Baseboard
* Two Allied Vision Alvium CSI cameras

Software-wise:

* Jetpack 4.4.1
* [My version](https://github.com/amarburg/linux_nvidia_jetson/tree/master_with_antmicro_patch) of Allied Vision's custom Kernel for CSI cameras
* Melodic [installed from debs](https://github.com/JetsonHacksNano/installROS)


It builds one executable `test_v4l_mwe`.   If the `NodeHandle` declaration on line 31 is *commented out*, the executable runs reliably:


```
aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Setting format for /dev/video0 to 1920 x 1216
Successfully created camera for /dev/video0 at 1920 x 1216

aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Setting format for /dev/video0 to 1920 x 1216
Successfully created camera for /dev/video0 at 1920 x 1216

aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Setting format for /dev/video0 to 1920 x 1216
Successfully created camera for /dev/video0 at 1920 x 1216
```

If the declaration is *enabled* then communication with the camera fails at a random `ioctl`.  As shown below, it succeeds once, then fails at two different `ioctls`.

```
aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Setting format for /dev/video0 to 1920 x 1216
Successfully created camera for /dev/video0 at 1920 x 1216

aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Setting format for /dev/video0 to 1920 x 1216
Error setting format for device /dev/video0(16):Device or resource busy

aaron@xaviernx ~/trisect_ws % rosrun trisect_v4l_mwe test_v4l_mwe
Error halting streaming for /dev/video0 (16):Device or resource busy 
```
